FROM docker:20

ARG BUILDX_URL=https://github.com/docker/buildx/releases/download/v0.5.1/buildx-v0.5.1.linux-amd64

# Enable experimental features in Docker client
RUN mkdir -p $HOME/.docker \
    && echo -e '{\n  "experimental": "enabled"\n}' | tee $HOME/.docker/config.json \
    && mkdir -p $HOME/.docker/cli-plugins/ \
    && wget -O $HOME/.docker/cli-plugins/docker-buildx $BUILDX_URL \
    && chmod a+x $HOME/.docker/cli-plugins/docker-buildx

# install additional packages
RUN apk add -u make
